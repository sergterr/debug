<?php

namespace Sergterr\Debug;

/**
 * TerrDebug Array Library
 *
 * Version 1.0.8
 */
class Debug
{
    /**
     * Time
     * Время
     *
     * @var array
     */
    protected static $time = array();

    /**
     * Memory
     *
     * @var array
     */
    protected static $memory = array();

    /**
     * Protected constructor since this is a static class.
     *
     * @access  protected
     */
    protected function __construct()
    {
        // Nothing here
    }

	/**
	 * Test Save data into file
	 *
	 * <code>
	 *  Debug::testSave($data);
	 * </code>
	 *
	 * @param $data
	 * @param string $path
	 */
	public static function testSave($data, string $path = '' ) {
		$path = !empty($path) ? rtrim($path, '/') . '/' : '';
		file_put_contents(
			$path . 'test_report.csv',
			print_r( $data, true ) . "\r\n",
			LOCK_EX | FILE_APPEND
		);
	}

    /**
     * Print the variable $data and exit if exit = true
     *
     * <code>
     *  Debug::printr($data);
     * </code>
     *
     * @param mixed   $data Data
     * @param boolean $exit Exit
     */
    public static function printr($data, bool $exit = false)
    {
        echo "<pre>dump \n---------------------- \n\n" .
            print_r($data, true) .
            "\n----------------------</pre>";
        if ($exit) {
            die();
        }
    }

    /**
     * Print the variable $data and exit if exit = true
     *
     * <code>
     *  Debug::dump($data);
     * </code>
     *
     * @param null $mixed
     * @param bool $die
     *
     * @return string
     */
    public static function dump($mixed = null, bool $die = true): string
    {
        ob_start();
        var_dump($mixed);
        $content = "<pre>dump \n---------------------- \n\n";
        $content .= ob_get_contents();
        $content .= "\n----------------------</pre>";
        ob_end_clean();

        if ($die) {
            die($content);
        }

        return $content;
    }
}
